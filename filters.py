from aupyom import Sampler, Sound
from aupyom.util import example_audio_file
import librosa

sampler = Sampler()

# Load sounds into the sampler
audio_file = "french_generic_version1.wav"
y, sr = librosa.load(audio_file)

# Shift the pitch of sounds
y_after_pitch = librosa.effects.pitch_shift(y, sr, n_steps=-3.0)

# Time-stretch the audio by a fixed rate.
y_final = librosa.effects.time_stretch(y_after_pitch, 0.8)

librosa.output.write_wav('french_generic_filters_version1.wav', y_final, sr)