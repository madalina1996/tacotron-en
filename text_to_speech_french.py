import os, requests, time
from xml.etree import ElementTree


class TextToSpeech(object):
    def __init__(self, subscription_key):
        self.subscription_key = subscription_key
        self.timestr = time.strftime("%Y%m%d-%H%M")
        self.access_token = None

    def set_text(self, tts):
        self.tts = tts

    def get_token(self):
        #https: // eastus.stt.speech.microsoft.com / speech / recognition / conversation / cognitiveservices / v1
        #https://api.cognitive.microsoft.com/sts/v1.0/issuetoken
        fetch_token_url = "https://westus.api.cognitive.microsoft.com/sts/v1.0/issueToken"
        headers = {
            'Ocp-Apim-Subscription-Key': self.subscription_key
        }
        response = requests.post(fetch_token_url, headers=headers)
        self.access_token = str(response.text)

    def save_audio(self, index):
        base_url = 'https://westus.tts.speech.microsoft.com/'
        path = 'cognitiveservices/v1'
        constructed_url = base_url + path
        headers = {
            'Authorization': 'Bearer ' + self.access_token,
            'Content-Type': 'application/ssml+xml',
            'X-Microsoft-OutputFormat': 'riff-24khz-16bit-mono-pcm',
            'User-Agent': 'YOUR_RESOURCE_NAME'
        }
        xml_body = ElementTree.Element('speak', version='1.0')
        xml_body.set('{http://www.w3.org/XML/1998/namespace}lang', 'fr-FR')
        voice = ElementTree.SubElement(xml_body, 'voice')
        voice.set('{http://www.w3.org/XML/1998/namespace}lang', 'fr-FR')
        voice.set('name', 'Microsoft Server Speech Text to Speech Voice (fr-FR, Paul, Apollo)')
        voice.text = self.tts
        body = ElementTree.tostring(xml_body)

        response = requests.post(constructed_url, headers=headers, data=body)
        if response.status_code == 200:
            with open('sample-' + str(index) + '.wav', 'wb') as audio:
                audio.write(response.content)
                print("\nStatus code: " + str(response.status_code) + "\nYour TTS is ready for playback.\n")
        else:
            print("\nStatus code: " + str(
                response.status_code) + "\nSomething went wrong. Check your subscription key and headers.\n")



if __name__ == "__main__":
    subscription_key = '113316d6f4ac4ab492cc026d3961b156'
    app = TextToSpeech(subscription_key)
    app.get_token()


    app.set_text("les annabelles ont accepté d'épouser un homme qu'elle n'avait rencontré qu'une fois depuis une demi-heure et qui vivait plus de quatre mille")
    app.save_audio(1)

    app.set_text("force de gravité lentement à travers la matière")
    app.save_audio(2)



